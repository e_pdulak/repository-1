def List results = 
[
    ['testKey':'01', 'status':'FAIL'],
    ['testKey':'02', 'status':'PASS'],
    ['testKey':'03', 'status':'PASS'],
    ['testKey':'04', 'status':'PASS'],
    ['testKey':'05', 'status':'PASS'],
    ['testKey':'06', 'status':'ABORTED'],
    ['testKey':'07', 'status':'PASS'],
    ['testKey':'08', 'status':'PASS'],
    ['testKey':'09', 'status':'FAIL'],
    ['testKey':'10', 'status':'PASS']
]
def Integer testPass = results.status.groupBy({​ it }​).collectEntries{​ k, v -> [ k, v.size() ] }​.find {​ it.key == "PASS" }​?.value?.toInteger()
def Double d = (0 < testPass?testPass:0) * 100.0 / (results.status.size()?.toInteger())
def Map r = [:]
r["testExeDate"] = "15. 04. 2021 17:29"
r["testPlanKey"] = "XRAY-000"
r["testEnvironments"] = "INT"
r["testExeSuccessRate"] = d.round(0)+"%"
r["testCount"] = results.status.size()?.toInteger()
r["testResults"] = results.status.groupBy({​ it }​).collectEntries{​ k, v -> [ k, v.size() ] }​.toMapString().replaceAll("\\:", ": ").replaceAll("\\[", "(").replaceAll("\\]",")")
def String report2xray = "${​r["testExeDate"]}​ byla dokončena exekuce test plánu ${​r["testPlanKey"]}​ na prostředí MSC ${​r["testEnvironments"]}​. Úspěšnost exekuce je ${​r["testExeSuccessRate"]}​. Počet provedených automatizovaných testů: ${​r["testCount"]}​ ${​r["testResults"]}​."
// 15. 04. 2021 17:29 byla dokončena exekuce test plánu XRAY-000 na prostředí MSC INT. Úspěšnost exekuce je 70.0%. Počet provedených automatizovaných testů: 10 (FAIL: 2, PASS: 7, ABORTED: 1).